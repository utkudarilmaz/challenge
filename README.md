# Challenge

## Deployment Crashloopback Issue

hello-kubernetes-first deployment and associated pod was broken.
The error message is "Liveness probe failed: Get
"http://10.244.1.38:80/": dial tcp 10.244.1.38:80: connect: connection
refused" where throwed on deployment details.

### Reason

In *hello-kubernetes-first* deployment, liveness probe port and container
port had setted **80** but the *hello-kubernetes* pod had starting to serve
http on port **8080**. So livenessProbe checks was failing because the http
requests sended to wrong port.

### Fix

For fixing this issue, liveness probe's port and container port setted to
**8080** that is actual serving port for the *hello-kubernetes* container.

```cmd
kubectl edit deployment hello-kubernetes-first -n default
```

## HPA Issue

```
Conditions:
  Type           Status  Reason                   Message
  ----           ------  ------                   -------
  AbleToScale    True    SucceededGetScale        the HPA controller was able to get the target's current scale
  ScalingActive  False   FailedGetResourceMetric  the HPA was unable to compute the replica count: failed to get cpu utilization: unable to get metrics for resource cpu: unable to fetch metrics from resource metrics API: the server is currently unable to handle the request (get pods.metrics.k8s.io)
Events:
  Type     Reason                   Age                       From                       Message
  ----     ------                   ----                      ----                       -------
  Warning  FailedGetResourceMetric  4m25s (x104597 over 18d)  horizontal-pod-autoscaler  failed to get cpu utilization: unable to get metrics for resource cpu: unable to fetch metrics from resource metrics API: the server is currently unable to handle the request (get pods.metrics.k8s.io)
```

### Reason

Horizontal pod autoscaler was defined well in the default namespace
for the *hello-kubernetes-first* deployment but the HPA had errors.

The errors are about the metrics server. HPA can not fetch the metrics
from metrics API because of metrics-server pods are not running on
the cluster.

### Fix

For fixing the issue, metrics server pods must be deployed on the
cluster and must provide the metrics via metrics API.

Therefore metrics-server pods was created on the *kube-system*
namespace. Than made sure pods are running well.

```

metrics-server-df6bf6869-k7k54     1/1     Running   0          9m26s
metrics-server-df6bf6869-l969j     1/1     Running   0          9m51s
```

When creating metrics-server deployment, for making metrics server HA,
2 replicas created for metrics-server. Also pod disruption budget was
defined that ensures min one replicas will running on the cluster.

Thus if any disaster scenario come true, metrics providers will be
continue to serving the metrics.

When creating metrics-server **bitnami/metrics-server** Helm package
was used.

```cmd
helm install metrics-server -f charts/metrics-server/values.yaml charts/metrics-server -n kube-system
```

Than finally hello-kubernetes-first deployment's HPA status is become
true.

```
Conditions:
  Type            Status  Reason              Message
  ----            ------  ------              -------
  AbleToScale     True    ReadyForNewScale    recommended size matches current size
  ScalingActive   True    ValidMetricFound    the HPA was able to successfully calculate a replica count from cpu resource utilization (percentage of request)
```

## Monitoring

### About

Prometheus is a monitoring and alerting toolkit. Prometheus collecting
the application metrics via jobs or exporters. The scraped metrics
stores on the Prometheus instances and producing some alerts which
defined by some rules.

For managing Kubernetes monitoring, the Prometheus operator,
Prometheus and Alert Manager have installed to cluster.

Also for collecting some metrics Kube State Metrics, Node Exporter
like metrics providers was installed.

Prometheus Operator is providing easy configuration and managing of the Prometheus
instance. The operator creates some CRDs like Prometheus, PrometheusRules
etc.

```cmd
$ kubectl api-resources | grep coreos.com
alertmanagerconfigs                               monitoring.coreos.com/v1alpha1         true         AlertmanagerConfig
alertmanagers                                     monitoring.coreos.com/v1               true         Alertmanager
podmonitors                                       monitoring.coreos.com/v1               true         PodMonitor
probes                                            monitoring.coreos.com/v1               true         Probe
prometheuses                                      monitoring.coreos.com/v1               true         Prometheus
prometheusrules                                   monitoring.coreos.com/v1               true         PrometheusRule
servicemonitors                                   monitoring.coreos.com/v1               true         ServiceMonitor
thanosrulers                                      monitoring.coreos.com/v1               true         ThanosRuler
```

### Installation

When installing the monitoring stack, Bitnami's kube-prometheus
Helm chart used. The Helm chart includes all stack of monitoring except
Grafana. The monitoring stack was installed on monitoring namespace.

```cmd

kubectl create namespace monitoring
helm install prometheus -f charts/kube-prometheus/values.yaml charts/kube-prometheus -n monitoring 
```

Also some basic alerts was defined on cluster thats are InstanceDown,
KubernetesStatefulSetDown, KubernetesPodCrashLooping etc.

You can find all defined alerts by `kubectl port-forward svc/prometheus-prometheus 9090:9090 -n monitoring
` and enter the [alerts page](http://localhost:9090/classic/alerts) on
browser.

Grafana is a visualazation tool which provides awesome dashboards.

When installing Grafana, Bitnami's Grafana Helm chart used.

```cmd
helm install grafana -f charts/grafana/values.yaml charts/grafana -n monitoring 
```

You can find admin password with `echo "Password: $(kubectl get secret grafana-admin --namespace monitoring -o jsonpath="{.data.GF_SECURITY_ADMIN_PASSWORD}" | base64 --decode)"`.

Some example dashboards added to Grafana. You can watch cluster status
from Grafana by `kubectl port-forward svc/grafana 3000:3000 -n
monitoring` and enter the [Node Exporter
Dashboard](http://localhost:3000/d/F2PvQDW7z/node-exporter-for-prometheus-dashboard?orgId=1).
The Node Exporter dashboard is using the collected metrics by Node
Exporters.

You can search and explore much and more dashboards from collected
metrics from explore tab on the Grafana.

## Logging

### About

When setting up Kubernetes centralized logging the Grafana’s Loki
storage and Fluent Bit log collector was used.

Loki is a log aggregation system inspired by Prometheus. Also lightweight
and easy to configure.

Fluent Bit is a log forwarder tool which allow to collect logs from
different resources and send to them to destinations. Fluent Bit is
designed with performance and high througput with low CPU and memory
usage.

### Architecture

Fluent Bit pods running as Daemonset on the cluster and collect every
pod logs from nodes where running on them. Collected logs parsed on
Fluent Bit instances and then forwarded to Loki instance. Than we can
watch live container logs on Grafana dashboards. 

### Installation

The logging stack was installed on *logging* namespace.

When installing both tools have used the Helm package manager.

```cmd

kubectl create namespace logging
helm install loki -f charts/loki/values.yaml charts/loki -n logging 
helm install fluent-bit -f charts/fluent-bit/values.yaml charts/fluent-bit -n logging 
```

A simple log dashboard was created on the Grafana. You can view the
example log dashboard running by 
`kubectl port-forward svc/grafana 3000:3000 -n monitoring` on
[Grafana](http://localhost:3000/d/Ffz4M5Z7z/logs?orgId=1&refresh=30s).

## Benchmarks

For make benchmark test to deployment wrk tool has been used.

```cmd

## 8 thread, 200 connection and 30 seconds test
$ wrk -t8 -c200 -d30s https://test.64.225.95.235.nip.io
Running 30s test @ https://test.64.225.95.235.nip.io
  8 threads and 200 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   793.91ms  126.59ms   1.40s    81.69%
    Req/Sec    41.20     35.49   191.00     79.49%
  7401 requests in 30.07s, 6.67MB read
Requests/sec:    246.09
Transfer/sec:    227.11KB

## 16 thread, 400 connection and 30 seconds test
$ wrk -t16 -c400 -d30s https://test.64.225.95.235.nip.io 
Running 30s test @ https://test.64.225.95.235.nip.io
  16 threads and 400 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   643.43ms  569.58ms   2.00s    61.81%
    Req/Sec    31.85     20.14   170.00     66.54%
  13595 requests in 30.09s, 12.20MB read
  Socket errors: connect 0, read 0, write 0, timeout 1621
  Non-2xx or 3xx responses: 96
Requests/sec:    451.79
Transfer/sec:    415.08KB

## 16 thread, 1000 connection and 30 seconds test
$ wrk -t16 -c1000 -d30s https://test.64.225.95.235.nip.io
Running 30s test @ https://test.64.225.95.235.nip.io
  16 threads and 1000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   768.51ms  568.35ms   2.00s    58.93%
    Req/Sec    37.21     22.91   170.00     71.67%
  15893 requests in 30.10s, 14.25MB read
  Socket errors: connect 0, read 0, write 0, timeout 4347
  Non-2xx or 3xx responses: 136
Requests/sec:    528.05
Transfer/sec:    484.68KB

## 16 thread, 2000 connection and 30 seconds test
$ wrk -t16 -c2000 -d30s https://test.64.225.95.235.nip.io
Running 30s test @ https://test.64.225.95.235.nip.io
  16 threads and 2000 connections
  Thread Stats   Avg      Stdev     Max   +/- Stdev
    Latency   949.69ms  612.57ms   2.00s    53.55%
    Req/Sec    48.10     35.87   414.00     71.90%
  17410 requests in 30.09s, 15.23MB read
  Socket errors: connect 995, read 0, write 0, timeout 5894
  Non-2xx or 3xx responses: 805
Requests/sec:    578.53
Transfer/sec:    518.32KB
```

## Cleaning

> Minimum Helm version is v3.1.0

```cmd

helm uninstall metrics-server -n kube-system
helm uninstall grafana -n monitoring
helm uninstall prometheus -n monitoring
kubectl delete namespace monitoring
helm uninstall fluent-bit -n logging
helm uninstall loki -n logging
kubectl delete namespace logging
```
